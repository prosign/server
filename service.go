package server

//go:generate mockgen -destination=./mock_server/mock_service.go -package=mock_server gitlab.com/prosign/server Service

// Service handles message sent to the server
type Service interface {
	Forward(string, []byte, Guest) ([]byte, error)
	OnGuestArival(Guest)
	OnGuestDeparture(Guest)
}
