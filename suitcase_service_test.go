// +build unit

package server_test

import (
	"testing"

	"github.com/golang/mock/gomock"
	. "gitlab.com/prosign/server"
	mock_server "gitlab.com/prosign/server/mock_server"
)

func TestCanCreateSuicaseService(t *testing.T) {
	if NewSuitcaseService() == nil {
		t.Error("Creating suitcase service returned nil")
	}
}

func TestCanStoreDataOnGuestAndGetItBackFromAnother(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockGuest := mock_server.NewMockGuest(ctrl)

	suitcaseService := NewSuitcaseService()

	response, err := suitcaseService.Forward("set", []byte("sampleKey:sampleValue"), mockGuest)
	if err != nil {
		t.Error(err)
	}

	if string(response) != "saved" {
		t.Errorf("Expected 'saved', got %s", response)
	}

	response, err = suitcaseService.Forward("get", []byte("sampleKey"), mockGuest)
	if err != nil {
		t.Error(err)
	}

	if string(response) != "sampleValue" {
		t.Errorf("Expected 'sampleValue', got %s", response)
	}

}
