package server

import (
	"log"
)

type terminalLogger struct {
	logs  []string
	debug bool
}

func (logger *terminalLogger) Log(entry string) {
	logger.logs = append(logger.logs, entry)
	if logger.debug {
		log.Print(entry)
	}
}

func NewTerminalLogger(debug bool) *terminalLogger {
	logger := &terminalLogger{
		debug: debug,
		logs:  make([]string, 0),
	}
	return logger
}
