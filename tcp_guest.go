package server

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"

	uuid "github.com/satori/go.uuid"
)

// Guest is a connection to our server
type tcpGuest struct {
	id     string
	roomID string
	reader *bufio.Reader
	writer *bufio.Writer
}

func (guest *tcpGuest) ID() string {
	return guest.id
}

// GetMessage recieves a message, if there is any, returning the header and body
func (guest *tcpGuest) GetMessage() (string, []byte, error) {

	sizeByte, err := guest.reader.ReadByte()
	if err != nil {
		return "", nil, err
	}
	bodySize := int(sizeByte)

	if bodySize < 0 {
		return "", nil, errors.New("Illegal body size: " + string(bodySize))
	}

	header, err := guest.reader.ReadString(':')
	if err != nil {
		return "", nil, err
	}
	headerSanitized := header[:len(header)-1]

	var bodyBuffer bytes.Buffer
	for i := 0; i < bodySize; i++ {
		token, err := guest.reader.ReadByte()
		if err != nil {
			return "", nil, err
		}
		bodyBuffer.WriteByte(token)
	}

	return headerSanitized, bodyBuffer.Bytes(), nil
}

func (guest tcpGuest) compose(statusByte byte, header string, body []byte) error {
	// Status Byte
	guest.writer.WriteByte(statusByte)

	// Length of body byte
	err := guest.writer.WriteByte(byte(len([]byte(body))))
	if err != nil {
		return err
	}

	_, err = guest.writer.WriteString(fmt.Sprintf("%s:", header))
	if err != nil {
		return err
	}

	_, err = guest.writer.Write(body)
	if err != nil {
		return err
	}

	err = guest.writer.Flush()
	if err != nil {
		return err
	}
	return nil
}

// SendMessage to the guest
func (guest tcpGuest) SendMessage(header string, body []byte) error {
	return guest.compose(byte(0), header, body)
}

func (guest tcpGuest) SendError(header string, err error) error {
	return guest.compose(byte(1), header, []byte(err.Error()))
}

func newTCPGuest(conn medium) (*tcpGuest, error) {
	if conn == nil {
		return nil, errors.New("Can't have a nil connection")
	}

	uuid, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}

	guest := &tcpGuest{
		id:     uuid.String(),
		roomID: "",
		reader: bufio.NewReader(conn),
		writer: bufio.NewWriter(conn),
	}

	return guest, nil
}
