package server

import (
	"errors"
	"fmt"
	"io"
	"net"
	"strings"
)

// Server manages the state of the app
type Server struct {
	services         map[string]Service
	messageSizeLimit int
	logger           Logger
}

// NewServer creates a server with default services room and suitcase
func NewServer(debug bool) *Server {
	server := &Server{
		services: make(map[string]Service),
		logger:   NewTerminalLogger(debug),
	}

	server.services["room"] = NewRoomService(server.logger)
	server.services["suitcase"] = NewSuitcaseService()
	return server
}

// NewEmptyServer creates a server with no services
func NewEmptyServer() *Server {
	server := &Server{
		services: make(map[string]Service),
		logger:   nil,
	}
	return server
}

// GetServices returns all services currently registered
func (server Server) GetServices() map[string]Service {
	return server.services
}

// RegisterService adds a service that a guest can interact with
func (server *Server) RegisterService(name string, service Service) error {
	if _, containsService := server.services[name]; containsService {
		return errors.New("Already contain service for name: " + name)
	}
	server.services[name] = service
	return nil
}

// Listen blocks and listens for incoming connections to the server
func (server Server) Listen(port string) {
	ln, err := net.Listen("tcp", port)
	if err != nil {
		panic(err.Error())
	}

	for {
		conn, err := ln.Accept()
		if err != nil {
			server.logger.Log(fmt.Sprintf("Error accepting socket: %s\n", err.Error()))
		}

		guest, err := newTCPGuest(conn)
		if err != nil {
			server.logger.Log(fmt.Sprintf("Error initializing guest data: %s", err.Error()))
			return

		}
		go server.handleNewGuest(guest)
	}
}

func (server Server) recieveMessageAndRespondToGuest(guest Guest) error {
	header, body, err := guest.GetMessage()

	if err != nil {
		return err
	}

	if strings.Index(header, ".") == -1 {
		server.logger.Log(fmt.Sprintf("Error recieving message: %s\n", "malformed header"))
		guest.SendError(header, errors.New("malformed header"))
		return nil
	}

	headerContents := strings.Split(header, ".")
	service := headerContents[0]
	method := headerContents[1]

	if val, ok := server.services[service]; ok {
		response, err := val.Forward(method, body, guest)
		if err != nil {
			return guest.SendError(header, err)
		} else if len(response) > 0 {
			return guest.SendMessage(header, response)
		}
	} else {
		return guest.SendError(header, errors.New("Server not configured with service"))
	}

	return nil
}

func (server Server) handleNewGuest(guest Guest) {

	for _, service := range server.services {
		service.OnGuestArival(guest)
	}

	for {
		err := server.recieveMessageAndRespondToGuest(guest)
		if err != nil {
			if err == io.EOF {
				server.logger.Log("Guest Disconnected")
			} else {
				server.logger.Log(fmt.Sprintf("Error recieving message: %s\n", err.Error()))
			}
			break
		}
	}

	for _, service := range server.services {
		service.OnGuestDeparture(guest)
	}

}
