package server

// Logger is an interface for intercepting logs from the server running
type Logger interface {
	Log(string)
}
