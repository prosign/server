// +build integration

package server_test

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"net"
	"testing"

	. "gitlab.com/prosign/server"
)

func TestCreatingEmptyServerHasNoServices(t *testing.T) {

	server := NewEmptyServer()

	if len(server.GetServices()) != 0 {
		t.Errorf("expecting emptry server to have 0 services, has %d instead", len(server.GetServices()))
	}
}

func TestGetServiceNotConfiguredMessageForEmptyServer(t *testing.T) {
	server := NewEmptyServer()

	port := ":1234"

	go server.Listen(port)

	// Conenct to our server
	conn, err := net.Dial("tcp", fmt.Sprintf("%s%s", "localhost", port))
	if err != nil {
		t.Error(err)
	}

	// Send message
	roomName := "My Room"
	writter := bufio.NewWriter(conn)
	writter.WriteByte(byte(len([]byte(roomName))))
	writter.WriteString("room.create:")
	writter.WriteString(roomName)
	writter.Flush()
	log.Println(byte(len([]byte(roomName))))

	// =========================== Recieve message ============================
	reader := bufio.NewReader(conn)

	// Read Status Byte ==============
	statusByte, err := reader.ReadByte()
	if err != nil {
		t.Error(err)
	}
	if int(statusByte) != 1 {
		t.Errorf("Unexpected status byte: %d", int(statusByte))
	}

	// Read Length Byte ===============
	sizeByte, err := reader.ReadByte()
	if err != nil {
		t.Error(err)
	}
	return

	bodySize := int(sizeByte)

	// Read Header ===============
	header, err := reader.ReadString(':')
	if err != nil {
		t.Error(err)
	}
	if header != "room.create" {
		t.Errorf("Unexpected header: %s", header)
	}

	// Read body ===============
	var bodyBuffer bytes.Buffer
	for i := 0; i < bodySize; i++ {
		token, err := reader.ReadByte()
		if err != nil {
			t.Error(err)
		}
		bodyBuffer.WriteByte(token)
	}

	if bodyBuffer.String() != "Server not configured with service" {
		t.Errorf("Unexpected body: %s", bodyBuffer.String())
	}

}

func TestServerCreatesRoom(t *testing.T) {
	server := NewServer(false)

	port := ":1235"

	go server.Listen(port)

	// Conenct to our server
	conn, err := net.Dial("tcp", fmt.Sprintf("%s%s", "localhost", port))
	if err != nil {
		t.Error(err)
	}

	// Send message
	roomName := "My Room"
	writter := bufio.NewWriter(conn)
	writter.WriteByte(byte(len([]byte(roomName))))
	writter.WriteString("room.create:")
	writter.WriteString(roomName)
	writter.Flush()
	log.Println(byte(len([]byte(roomName))))

	// =========================== Recieve message ============================
	reader := bufio.NewReader(conn)

	// Read Status Byte ==============
	statusByte, err := reader.ReadByte()
	if err != nil {
		t.Error(err)
	}
	if int(statusByte) != 0 {
		t.Errorf("Unexpected status byte: %d", int(statusByte))
	}

	// Read Length Byte ===============
	sizeByte, err := reader.ReadByte()
	if err != nil {
		t.Error(err)
	}
	return

	bodySize := int(sizeByte)

	// Read Header ===============
	header, err := reader.ReadString(':')
	if err != nil {
		t.Error(err)
	}
	if header != "room.create" {
		t.Errorf("Unexpected header: %s", header)
	}

	// Read body ===============
	var bodyBuffer bytes.Buffer
	for i := 0; i < bodySize; i++ {
		token, err := reader.ReadByte()
		if err != nil {
			t.Error(err)
		}
		bodyBuffer.WriteByte(token)
	}

	if len(bodyBuffer.String()) != 36 {
		t.Errorf("Unexpected body: %s", bodyBuffer.String())
	}

}
