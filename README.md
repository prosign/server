# Prosign

[![pipeline status](https://gitlab.com/prosign/server/badges/master/pipeline.svg)](https://gitlab.com/prosign/server/commits/master) [![coverage report](https://gitlab.com/prosign/server/badges/master/coverage.svg)](https://gitlab.com/prosign/server/commits/master)

Small extendable tcp messaging server with a focus on simple protocols.

```
<length of body byte (uint8)><service>.<method>:<body>
```

# Default Services

## Room

### Methods

#### Join

Example: 

```
$room.join:2cf47321-d837-4ccc-a52f-e2aa01103b86
```


(`System.Text.Encoding.UTF8.GetString(new byte[]{Convert.ToByte("2cf47321-d837-4ccc-a52f-e2aa01103b86".Length)}) == "$"`)

#### Create

Create a room with a certain display name. If no display name is provided then the room is unlisted.

Returns uuid of the room that can be used to connect with

Example: 

```
room.create:name
```

Returns:

```
room.create:
```

#### Disconnect

Leave the room your currently in.

```
room.disconnet:
```

#### Update

```
*room.update:Data that is sent to everyone in the room!
```
