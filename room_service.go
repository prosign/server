package server

import (
	"errors"
	"fmt"
)

type roomServiceMiddleware func([]byte, Guest) ([]byte, error)

// RoomService manages sending updates to guests in the same room
type RoomService struct {
	hotel                    *Hotel
	logger                   Logger
	incomingUpdateMiddleware []roomServiceMiddleware
}

// NewRoomService creates a new RoomService with no middleware
func NewRoomService(logger Logger) *RoomService {
	service := &RoomService{
		hotel:  NewHotel(),
		logger: logger,
		incomingUpdateMiddleware: make([]roomServiceMiddleware, 0),
	}
	return service
}

func (service RoomService) listRooms() (string, error) {
	return service.hotel.listRooms(), nil
}

func (service RoomService) onRoomConnect(body string, guest Guest) (string, error) {
	err := service.hotel.CheckGuestIntoRoom(body, guest)
	if err != nil {
		return "", err
	}
	return "connected", nil
}

func (service RoomService) onRoomDisconnect(guest Guest) (string, error) {
	err := service.hotel.CheckGuestOutOfHotel(guest)
	if err != nil {
		return "", err
	}
	return "success", nil
}

func (service RoomService) onRoomUpdate(body []byte, guest Guest) error {
	newMessage := body
	var err error
	for _, middleware := range service.incomingUpdateMiddleware {
		newMessage, err = middleware(newMessage, guest)
		if err != nil {
			return err
		}
	}

	if len(newMessage) == 0 {
		return nil
	}

	service.logger.Log(string(newMessage))
	return service.hotel.UpdateRoom(guest, newMessage)
}

func (service RoomService) onRoomCreate(displayName string, guest Guest) (string, error) {
	service.logger.Log(fmt.Sprintf("Room Created (%s)", displayName))
	_, id, err := service.hotel.CreateRoom(displayName, guest)
	if err != nil {
		return "", err
	}
	return id, nil
}

// InterceptIncomingUpdate registers middleware that will be called whenever a
// guest sends an update, allowing modifications to be made before the update
// is sent to the rest of the room.
func (service *RoomService) InterceptIncomingUpdate(middleware func([]byte, Guest) ([]byte, error)) {
	service.incomingUpdateMiddleware = append(service.incomingUpdateMiddleware, middleware)
}

// Forward sends messages to the room service
func (service RoomService) Forward(method string, body []byte, guest Guest) ([]byte, error) {
	service.logger.Log(fmt.Sprintf("%s: %s", method, body))

	stringBody := string(body)

	response := ""
	err := errors.New("Unrecognized command")
	switch method {

	case "create":
		response, err = service.onRoomCreate(stringBody, guest)

	case "join":
		response, err = service.onRoomConnect(stringBody, guest)

	case "leave":
		response, err = service.onRoomDisconnect(guest)

	case "update":
		err = service.onRoomUpdate(body, guest)

	case "list":
		response, err = service.listRooms()

	}
	return []byte(response), err
}

func (service RoomService) OnGuestArival(Guest) {

}

func (service RoomService) OnGuestDeparture(guest Guest) {
	service.logger.Log("Guest Left")
	err := service.hotel.CheckGuestOutOfHotel(guest)
	if err != nil {
		service.logger.Log(fmt.Sprintf("Error Leaving Room: %s", err.Error()))
	}
}
