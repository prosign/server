package server

import (
	"errors"
	"strings"
)

// SuitcaseService manages data set by the guest themselves
type SuitcaseService struct {
	suitcases map[Guest]map[string]string
}

// NewSuitcaseService builds a new instance of the suitcase service
func NewSuitcaseService() *SuitcaseService {
	var service = &SuitcaseService{
		suitcases: make(map[Guest]map[string]string),
	}
	return service
}

func (service *SuitcaseService) saveValueForGuest(message string, guest Guest) error {
	contents := strings.Split(message, ":")

	if _, guestHasSuitcase := service.suitcases[guest]; guestHasSuitcase == false {
		service.suitcases[guest] = make(map[string]string)
	}

	service.suitcases[guest][contents[0]] = contents[1]
	return nil
}

func (service SuitcaseService) getValueForGuest(message string, guest Guest) (string, error) {
	if suitcase, guestHasSuitcase := service.suitcases[guest]; guestHasSuitcase {
		if value, suitcaseContainsValue := suitcase[message]; suitcaseContainsValue {
			return value, nil
		}
	}
	return "", errors.New("Guest does not have value")
}

func (service SuitcaseService) Forward(method string, body []byte, guest Guest) ([]byte, error) {

	bodyString := string(body)

	returnMessage := ""
	err := errors.New("Method does not exist")

	switch method {

	case "set":
		err = service.saveValueForGuest(bodyString, guest)
		if err == nil {

			returnMessage = "saved"
		}

	case "get":
		returnMessage, err = service.getValueForGuest(bodyString, guest)

	}

	return []byte(returnMessage), err
}

func (service SuitcaseService) OnGuestArival(Guest) {

}

func (service SuitcaseService) OnGuestDeparture(guest Guest) {

}
